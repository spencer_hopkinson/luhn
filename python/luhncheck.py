def LuhnCheck(cardNumber):            
    sum = 0
    parity = len(cardNumber) % 2
    
    for count in range(len(cardNumber)-1,0-1,-1):
        digit = int(cardNumber[count])

        if (parity == count % 2):        
            if (digit * 2 >= 10):
                sum += 1 + (digit * 2 % 10)
            else:                
                sum += digit * 2
        else:        
            sum += digit
    return (sum % 10 == 0)
