import sys
import luhncheck

#print("arg0: %s" % sys.argv[0])

luhncheck.LuhnCheck("2134")
luhncheck.LuhnCheck(sys.argv[1])

cardNumbers = [
                "4111111111111111", #VISA
                "5105105105105100", #Mastercard
                "340000000000009",  #AMEX
                "6011000000000004", #Discover
                "30000000000004",   #Diners Club
                "3569990010095841", #JCB
                "4111111111111112", #INVALID
                "5105105105105103", #INVALID
                "340000000000004",  #INVALID
                "6011000000000005", #INVALID
                "30000000000006",   #INVALID 
                "3569990010095847"  #INVALID
            ]

for cardNumber in cardNumbers:    
    print(f"Card Number: {cardNumber} IsValid:{luhncheck.LuhnCheck(cardNumber)}")