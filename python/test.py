import unittest
import luhncheck

class LunchCheckTest(unittest.TestCase):
    def test_Valid_EvenNumber_Returns_True(self):
        result = luhncheck.LuhnCheck("4111111111111111") #even numbered Visa
        assert result == True

    def test_Valid_NotEvenNumber_Returns_True(self):
        result = luhncheck.LuhnCheck("340000000000009") #even numbered Amex
        assert result == True

    def test_NotValid_Number_Returns_False(self):
        result = luhncheck.LuhnCheck("123") 
        assert result == False