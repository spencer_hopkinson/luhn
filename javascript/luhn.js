module.exports = function LuhnCheck(cardNumber)
{
    /* LUHN check
        * start from right checksum digit working right to left
        * multiply every other number by 2
        * if sum of multiplcation > 10, add individual digits together
        * add all numbers
        * if mod 10 - is valid
    */
    
    let sum = 0;
    let parity = (cardNumber.length) % 2; // is it odd or even length
    
    for(let count = cardNumber.length-1; count >=0; count--)
    {
        let digit = parseInt(cardNumber[count]);

        if (parity == count % 2)
        {
            if (digit * 2 >= 10)
            {
                sum += 1 + (digit * 2 % 10);
            }
            else
            {
                sum += digit * 2;
            }
        }
        else
        {
            sum += digit;
        }                
    }

    result = (sum % 10 == 0);    
    
    return result;
}        