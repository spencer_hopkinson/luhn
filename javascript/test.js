const assert = require("assert");
const luhn = require("./luhn");

describe("LUHN Test", ()=>{
    it("Returns true for a valid card number of 16 chars", ()=>{
        assert(luhn("4111111111111111")==true);
    }),
    it("Returns true for a valid card number of 15 chars", ()=>{
        assert(luhn("340000000000009")==true);
    }),
    it("Returns false for an invalid card number", ()=>{
        assert(luhn("340000000000001")==false);
    })
})