public class Luhn
{
    public static bool LuhnCheck(string cardNumber)
    {
        /* LUHN check
            * start from right checksum digit working right to left
            * multiply every other number by 2
            * if sum of multiplcation > 10, add individual digits together
            * add all numbers
            * if mod 10 - is valid
        */

        int sum = 0;
        int parity = (cardNumber.Length) % 2; // is it odd or even length
        
        for(int count = cardNumber.Length-1; count >=0; count--)
        {
            int digit = int.Parse(cardNumber[count].ToString());

            if (parity == count % 2)
            {
                if (digit * 2 >= 10)
                {
                    sum += 1 + (digit * 2 % 10);
                }
                else
                {
                    sum += digit * 2;
                }
            }
            else
            {
                sum += digit;
            }                
        }

        return (sum % 10 == 0);
    }
}