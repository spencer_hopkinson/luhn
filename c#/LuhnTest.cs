using Xunit;

public class LuhnTest
{
    [Fact]
    public void Valid_CardNumber_Returns_True()
    {
        Assert.True(Luhn.LuhnCheck("4111111111111111"));
    }

    [Fact]
    public void Valid_CardNumber_Returns_True_For_OddNumber_Length()
    {
        Assert.True(Luhn.LuhnCheck("340000000000009"));
    }

    [Fact]
    public void Valid_CardNumber_Returns_False_For_Invalid_Card_Number()
    {
        Assert.False(Luhn.LuhnCheck("340000000000001"));
    }
}