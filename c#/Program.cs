﻿using System;
using System.Linq;

namespace LuhnCheck
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] cardNumbers =
            {
                "4111111111111111", // VISA
                "5105105105105100", // Mastercard
                "340000000000009",  // AMEX
                "6011000000000004", // Discover
                "30000000000004",   // Diners Club
                "3569990010095841", // JCB
                "4111111111111112", // INVALID
                "5105105105105103", // INVALID
                "340000000000004",  // INVALID
                "6011000000000005", // INVALID
                "30000000000006",   // INVALID 
                "3569990010095847"  // INVALID
            };

            foreach(string card in cardNumbers)
            {
                Console.WriteLine($"Card Number: {card} IsValid: {Luhn.LuhnCheck(card)}");
            }
        }

        //Someone else wrote this super succinct version, used to validate my own!
        private static bool IsValid2(string cardNumber)
        {
            int sumOfDigits = cardNumber
                .Where((e) => e >= '0' && e <= '9')
                .Reverse()
                .Select((e, i) => (e - 48) * (i % 2 == 0 ? 1 : 2))
                .Sum((e) => e / 10 + e % 10);

            return sumOfDigits % 10 == 0;
        }
    }
}
