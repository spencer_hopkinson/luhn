. ./build.ps1

#Copy-Item -Path "./jar/*.jar" -Destination "./bin"

Set-Location "./bin"

Write-Host "Running tests..." -ForegroundColor Yellow
java -cp "../jar/junit-4.13.2.jar;../jar/hamcrest-core-1.3.jar;" org.junit.runner.JUnitCore com.luhn.LuhnTest

Set-Location ..