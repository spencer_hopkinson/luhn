Clear-Host

Remove-Item -Path "./bin" -Recurse

Write-Host "Compiling..." -ForegroundColor Yellow

javac -cp ./jar/junit-4.13.2.jar *.java -d "./bin/"

Write-Host "Compile done" -ForegroundColor Green