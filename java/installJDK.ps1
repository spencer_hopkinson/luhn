invoke-webrequest "https://download.oracle.com/java/17/latest/jdk-17_windows-x64_bin.zip" `
    -OutFile "jdk_17_bin.zip"
 
Expand-Archive -Path "jdk_17_bin.zip" -DestinationPath "c:\java\jdk-17"