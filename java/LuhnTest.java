package com.luhn;   

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import org.junit.*;

public class LuhnTest{

    @Test
    public void Valid_CardNumber_Returns_True(){
        assertTrue(Luhn.LuhnCheck("4111111111111111"));
    }

    @Test
    public void Valid_CardNumber_Returns_True_For_OddNumber_Length(){
        assertTrue(Luhn.LuhnCheck("340000000000009"));
    }

    @Test
    public void Valid_CardNumber_Returns_False_For_Invalid_Card_Number(){
        assertFalse(Luhn.LuhnCheck("340000000000001"));
    }
}