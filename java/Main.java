package com.luhn;

public class Main
{
    public static void main(String args[])
    {
        String[] cardNumbers =
            {
                "4111111111111111", // VISA
                "5105105105105100", // Mastercard
                "340000000000009",  // AMEX
                "6011000000000004", // Discover
                "30000000000004",   // Diners Club
                "3569990010095841", // JCB
                "4111111111111112", // INVALID
                "5105105105105103", // INVALID
                "340000000000004",  // INVALID
                "6011000000000005", // INVALID
                "30000000000006",   // INVALID 
                "3569990010095847"  // INVALID
            };

            for(String card:cardNumbers)
            {
                System.out.println(String.format("Card Number: %s IsValid: %s",card, Luhn.LuhnCheck(card)));                
            }
    }
}